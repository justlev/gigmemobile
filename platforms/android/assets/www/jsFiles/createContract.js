/**
 * Created by lev on 26/01/16.
 */

var proposeTo;

function createProposeAndGo(user){
    proposeTo = user;
    var nextpage = "./contractCreation.html";
    // swipe using id of next page if exists
    setScreenId("contractCreation");

    $.mobile.changePage(nextpage, {transition: "none", reverse: false}, true, false);
    //}
    event.handled = true;

}

function handleContractCreationPage(){
    $('#contractBetweet').text('This is a contract between '+getJData().Name+' and '+proposeTo.Name);
    $('#descClientName').text(proposeTo.Name+' will be ');
    $('#descOwnerName').text('for '+getJData().Name);

}

function proposeContract(){
    var text = $('#descClientName').text()+ ' '+$('#actionType').val()+' '+$('#descOwnerName').text()+' at '+$('#dateGig').val()+
            ' in the location '+$('#eventAddress').text();

    var dataToSend = {
      title:$('#contractName').val(),
        date:$('#dateGig').val(),
        description:$('#contractBetweet').text(),
        content:text,
        price:$('#contractAmount').val(),
        creatorID:getJData().ID,
        clientID:proposeTo.ID

    };
    $.ajax({
        method:'POST',
        url:server+'/GigMe/api/contracts/createNewContract',
        data:JSON.stringify(dataToSend),
        success:function(){
            goToContracts();
        },
        error:function(xhr,data,status) {
            alert('error!');
            console.log(xhr);
            console.log(data);
            console.log(status);
        }

    });
}
