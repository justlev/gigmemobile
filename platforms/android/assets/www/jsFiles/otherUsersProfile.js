/**
 * Created by lev on 23/01/16.
 */

var selectedProfile;
function setSelectedProfile(profile){
    selectedProfile = profile;
}

function getSelectedProfile(){
    return selectedProfile;
}

function goToSelectedProfile(profile){
    if (profile.ID==getJData().ID){
        goToProfile();
        return;
    }
    setSelectedProfile(profile);
    var nextpage = "./othersProfile.html";
    // swipe using id of next page if exists
    setScreenId("othersProfile");

    $.mobile.changePage(nextpage, {transition: "none", reverse: false}, true, false);
    //}
    event.handled = true;
}

function addUserToMyBand(){
    var nextpage = "./addUserToBand.html";
    // swipe using id of next page if exists
    setScreenId("addUserToBand");

    $.mobile.changePage(nextpage, {transition: "none", reverse: false}, true, false);
    //}
    event.handled = true;

}
var myBandsOptions = [];

function handleAddToBand(){
        $('#otherUserN').text(selectedProfile.Name);
    $('#otherUserProff').attr('src',server+'/GigMe/api/user/getUsersImage/'+selectedProfile.ID)
    $.ajax({
        url:server+'/GigMe/api/bands/getBandsByOwnerWithoutMember/'+getJData().ID+'/'+selectedProfile.ID,
        method:'GET',
        success:function(data,status,xhr){
            myBandsOptions = data;
            console.log("Bands!!!!");
            console.log(data);
            for (var i=0;i<data.length;i++){
                var element = "<li name='usersBand' onclick='addToThisBand(this)' id='"+i+"'><img class='band' src='"+server+"/GigMe/api/bands/getBandsImage/"+data[i].BandID+"'></li>";
                if (i==0){
                    $('#bandsOptionsUL').append(element)
                }
                else{
                    $('#bandsOptionsUL').children().last().after(element);
                    $('#bandsOptionsUL').children().last().on('touchend', function(){
                        addToThisBand(this);
                    });
                }


            }

        },
        error:function(data,status,xhr){
            alert('server error when getting bands')
        }
    });
}

function addToThisBand(elem){
    var index = parseInt($(elem).attr('id'));
    var bandToAddToID = myBandsOptions[index].BandID;
    var result = confirm('Add '+selectedProfile.Name+' to '+myBandsOptions[index].BandName+' ?');
    if (result){
        $.ajax({
            url:server+'/GigMe/api/bands/addUserToBand/'+bandToAddToID+'/'+selectedProfile.ID,
            method:'POST',
            success:function(data,status,xhr){
                goToSelectedProfile(selectedProfile);
            },
            error:function(data,status,xhr){
                alert('server error when getting bands')
            }
        });
    }



}

function handleOthersUserProfile(){
    $('#profilePic').attr('src',server+'/GigMe/api/user/getUsersImage/'+getSelectedProfile().ID);
    $('#profileName').text(getSelectedProfile().Name);
    var instruments = getSelectedProfile().Instruments;
    var instrumentsStr = "";
    for (var i=0;i<instruments.length;i++){
        instrumentsStr+=instruments[i];
        if (i!=instruments.length-1){
            instrumentsStr+=", ";
        }
    }
    $('#instruments').text(instrumentsStr);
    $('#availableLocation').text(getSelectedProfile().Address);

    $.ajax({
        url:server+'/GigMe/api/bands/getBands/'+getSelectedProfile().ID,
        method:'GET',
        success:function(data,status,xhr){
            bands = data;
            console.log("Bands!!!!");
            console.log(data);
            for (var i=0;i<data.length;i++){
                var element = "<li name='usersBand' onclick='clickedOnBand(this)' id='"+i+"'><img class='band' src='"+server+"/GigMe/api/bands/getBandsImage/"+data[i].BandID+"'></li>";
                if (i==0){
                    if (data[i].Owner.ID==getSelectedProfile().ID){
                        $('#hisBands').append(element)
                    }
                    else{
                        $('#inBands').append(element)
                    }

                }
                else{

                    if (data[i].Owner.ID==getSelectedProfile().ID){
                        $('#hisBands').children().last().after(element);
                    }
                    else{
                        $('#inBands').children().last().after(element);
                    }

                    /*$('#bands').children().last().on('touchend', function(){
                        clickedOnBand(this);
                    });*/
                }


            }

        },
        error:function(data,status,xhr){
            alert('server error when getting bands')
        }
    });

    $.ajax({
        url:server+'/GigMe/api/bands/getBandsByMember/'+getSelectedProfile().ID,
        method:'GET',
        success:function(data,status,xhr){
            bands = data;
            console.log("Bands!!!!");
            console.log(data);
            for (var i=0;i<data.length;i++){
                var element = "<li name='usersBand' onclick='clickedOnBand(this)' id='"+i+"'><img class='band' src='"+server+"/GigMe/api/bands/getBandsImage/"+data[i].BandID+"'></li>";
                if (i==0){
                    if (data[i].Owner.ID==getSelectedProfile().ID){
                        $('#inBands').append(element)
                    }
                    else{
                        $('#inBands').append(element)
                    }

                }
                else{

                    if (data[i].Owner.ID==getSelectedProfile().ID){
                        $('#inBands').children().last().after(element);
                    }
                    else{
                        $('#inBands').children().last().after(element);
                    }

                    /*$('#bands').children().last().on('touchend', function(){
                     clickedOnBand(this);
                     });*/
                }


            }

        },
        error:function(data,status,xhr){
            alert('server error when getting bands')
        }
    });

    $.ajax({
        url:server+'/GigMe/api/media/getUsersMedia/'+getSelectedProfile().ID,
        method:'GET',
        success:function(data,status,xhr){
            console.log("Medias!!!!!");

            console.log(data);
            for (var i=0;i<data.length;i++){

                var mediaPlayer = "SoundCloudPlayer";
                if (data[i].mediaPath.toLowerCase().indexOf("bandcamp")>0){
                    mediaPlayer = "https://bandcamp.com/EmbeddedPlayer/track=2717629732/size=small/bgcol=ffffff/linkcol=7137dc/artwork=none/transparent=true/";
                }

                $('#postContainerUL').append("<li name='usersMedia'>" +
                    "<div id='media"+i+"' class='post'>" +
                    "<p>"+data[i].mediaTitle+"</p>"+
                    '<iframe style="border: 0; width: 100%; height: 42px;" src="'+mediaPlayer+'" seamless><a href="'+data[i].mediaPath+'">'+data[i].mediaTitle+'</a></iframe>'+
                    "</div>" +
                    "</li>")
            }

        },
        error:function(data,status,xhr){
            alert('server error when getting media!')
        }
    });




}

function clickedOnBand(b){
    selectedBand = bands[parseInt($(b).attr('id'))];
    goToBands();
}

function messageUser(){

    $.ajax({
        url:server+'/GigMe/api/conversations/getConversationIDByMembers/'+getSelectedProfile().ID+"/"+getJData().ID,
        method:'GET',
        success:function(data,status,xhr){
            console.log(data);
            getMessageFromExternal(data);

        },
        error:function(data,status,xhr){
          alert('errorgetting or creating conversation between users')
        }
    });

}

function proposeAContract(){
        createProposeAndGo(selectedProfile);
}