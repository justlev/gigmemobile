/**
 * Created by 8o8inSquares on 1/17/2016.
 */

conversationIndex=0;
conversations=0;
thisConversation=undefined;
thisConversationMessagesCount=0;

function goToMessages(){
    //if(getScreenId()=="profilePage") {
        var nextpage = "./messages.html";
        // swipe using id of next page if exists

        $.mobile.changePage(nextpage, {transition: "none", reverse: false}, true, false);
        setScreenId("messagesPage");
    //}
    event.handled = true;
}

function getMessage(c){
    //if(getScreenId()=="messagesPage") {
        var conversationID= parseInt($(c).attr("id"));
        thisConversation = conversations[conversationID];
        thisConversationMessagesCount = thisConversation.Messages.length;
        conversationIndex = conversationID;

        var nextpage = "./conversation.html";
        // swipe using id of next page if exists

        $.mobile.changePage(nextpage, {transition: "none", reverse: false}, true, false);
        setScreenId("conversationPage");
 //   }
    event.handled = true;

}

function getMessageFromExternal(thiConversation){
    thisConversation = thiConversation;
    thisConversationMessagesCount = thisConversation.Messages.length;
    conversationIndex = 0;
    conversations=[thiConversation];

    var nextpage = "./conversation.html";
    // swipe using id of next page if exists

    $.mobile.changePage(nextpage, {transition: "none", reverse: false}, true, false);
    setScreenId("conversationPage");
    //   }
    event.handled = true;
}

function handleConversationPage(){

    for (var i=0;i<thisConversation.Messages.length;i++){

        var backgroundColor="rgba(127, 249, 125, 0.49);";
        if (jdata.ID != thisConversation.Messages[i].Composer.ID){
            backgroundColor="rgba(141, 247, 198, 0.25);"
        }
        var element = "<div id='"+thisConversation.Messages[i].MessageID+"' class='msgBlock' style='background-color:"+backgroundColor+"'>" +
            "<h4 class='messageComposer'>"+thisConversation.Messages[i].Composer.Name+"</h4>" +
            "<h4 class='messageDate'>"+thisConversation.Messages[i].Date+"</h4>" +
            "<p>"+thisConversation.Messages[i].Text+"</p>" +
            "</div> ";
        if (i==0){
            $('#msgsLs').append(element);
        }
        else{
            $('#msgsLs').children().last().after(element);
        }

    }
    leftConversationPage = false;
    loadMessagesLoop();
}

function handleMessagesPage(){
    //$('#messagesList')f
    $.ajax({
        url:server+'/GigMe/api/conversations/getUsersConversation/'+jdata.ID,
        method:'GET',
        success:function(data,status,xhr){
            conversations = data;
            console.log("Messages!!!!");
            console.log(data);
            for (var i=0;i<data.length;i++){
                var titleToUse = data[i].Receiver.Name;
                if (titleToUse==jdata.Name){
                    titleToUse = data[i].Owner.Name;
                }
                if (data[i].Messages.length>0){
                    var message = data[i].Messages[data[i].Messages.length-1];
                    var splitUntil = 20;
                    if (splitUntil>message.length){
                        splitUntil = message.length;
                    }
                    $('#messagesList').append("<li name='message' class='message' onclick='getMessage(this)' id='"+[i]+"'>" +
                        "<div>" +
                        "<h2 class='messageTitle'>"+titleToUse+"</h2> <br />" +
                        "<p>"+message.Text.substring(0,splitUntil)+"..."+"</p>"+
                        "</div></li>")
                }

            }

        },
        error:function(data,status,xhr){
            alert('server error when getting messages')
        }
    });

    var msgs = document.getElementsByName('message');
    for (var i=0;i<msgs.length;i++){
        $(msgs[i]).on('touchend', function(){getMessage(this)});
    }
}


sendMessage = function(){
    var text = $('#responseInput').val();
    var receiverID = thisConversation.Receiver.ID;
    if (receiverID==jdata.ID){
        receiverID = thisConversation.Owner.ID;
    }

    $.ajax({
        url:server+'/GigMe/api/conversations/sendMessage',
        data:JSON.stringify({ownerID:jdata.ID,recieverID:receiverID,conversationID:thisConversation.ConversationID,messageText:text}),
        method:'POST',
        success:function(data,status,xhr){
            thisConversationMessagesCount++;
            var date = moment().format("DD/MM/YYYY HH:mm:ss");
            var id = moment().format("DDMMYYYYHHmmss");
            console.log("sendMessage!!!!!");
            element = "<div id='"+id+"' class='msgBlock' style='display:none; background-color:rgba(127, 249, 125, 0.49);'>" +
                "<h4 class='messageComposer'>"+jdata.Name+"</h4>" +
                "<h4 class='messageDate'>"+date+"</h4>" +
                "<p>"+text+"</p>" +
                "</div> ";
            $('#msgsLs').children().last().after(element);
            $('#'+id).fadeIn('fast');
            $('#responseInput').val('');

        },
        error:function(data,status,xhr){
            alert('server error when getting media!')
        }
    });

};

var leftConversationPage=true;

loadMessagesLoop = function(){

    $.ajax({
        url:server+'/GigMe/api/conversations/getUnreadMessagesInConversation/'+jdata.ID+"/"+thisConversation.ConversationID,
        method:'GET',
        async:false,
        success:function(data,status,xhr){
            var unreadCount =parseInt(data);
            if (unreadCount>thisConversationMessagesCount){
                $.ajax({
                    url:server+'/GigMe/api/conversations/getUsersConversation/'+jdata.ID,
                    method:'GET',
                    async:false,
                    success:function(data,status,xhr){
                        var oldMessagesCount = thisConversation.Messages.length;
                        conversations = data;
                        for (var i=0;i<conversations.length;i++){
                            if (conversations[i].ConversationID==thisConversation.ConversationID){
                                thisConversation = conversations[i];
                                break;
                            }
                        }

                        console.log("Messages!!!!");
                        console.log(data);
                        for (var i=oldMessagesCount-1;i<thisConversation.Messages.length;i++){
                            var alreadyHere = $('#'+thisConversation.Messages[i].MessageID);
                            if (alreadyHere.length<=0) {
                                var backgroundColor="rgba(127, 249, 125, 0.49);";
                                if (jdata.ID != thisConversation.Messages[i].Composer.ID){
                                    backgroundColor="rgba(141, 247, 198, 0.25);"
                                }

                                var element = "<div id='" + thisConversation.Messages[i].MessageID + "' class='msgBlock' style='background-color:"+backgroundColor+"'>" +
                                    "<h4 class='messageComposer'>" + thisConversation.Messages[i].Composer.Name + "</h4>" +
                                    "<h4 class='messageDate'>" + thisConversation.Messages[i].Date + "</h4>" +
                                    "<p>" + thisConversation.Messages[i].Text + "</p>" +
                                    "</div> ";
                                $('#msgsLs').children().last().after(element);
                            }


                        }
                        thisConversationMessagesCount = thisConversation.Messages.length;

                    },
                    error:function(data,status,xhr){
                        alert('server error when getting messages')
                    }
                });
            }
            setTimeout(function(){
                if (!leftConversationPage){
                    loadMessagesLoop();
                }
            },2000)


        },
        error:function(data,status,xhr){
            alert('server error when getting media!')
        }
    });

};

exiting= function(){
    leftConversationPage = true;
    console.log('exiting conversation page.')
}