/**
 * Created by lev on 18/01/16.
 */

var thoseContracts;
var selectedContract;
var createdContracts;

function handleContractsPage(){
    $.ajax({
        url:server+'/GigMe/api/contracts/getContractsProposedToUser/'+getJData().ID,
        method:'GET',
        success:function(data,status,xhr){
            thoseContracts = data;
            console.log("Contracts!!!!!");

            console.log(data);
            for (var i=0;i<data.length;i++){
                var color = 'rgba(0,0,0,0);';
                if (data[i].CreatorSigned.toString()=='true' && data[i].ClientSigned.toString()=='false'){
                    color = 'rgba(224, 249, 125, 0.49);'
                }
                else if (data[i].CreatorSigned.toString()=='true' && data[i].ClientSigned.toString()=='true'){
                    color = 'rgba(127, 249, 125, 0.49);';
                }
                var element = "<li name='contract' id='"+i+"' onclick='goToSpecificContract(this)' class='msgBlock' style='background-color:"+color+"'>" +
                    "<div>" +
                    "<h2>"+data[i].ContractTitle+"</h2>"+
                    "<p style='float:right;'> Proposed By "+data[i].Creator.Name+"</p>"+
                    "<p>"+data[i].ContractDescription+"</p>"+

                    "<p style='float:right; color:lawngreen'>"+data[i].Price+"</p>"+
                    "<p >"+data[i].ContractTime+"</p>"+
                    "</div>" +
                    "</li>";
                if (i==0){
                    $('#contractsContainer').append(element);
                }
                else{
                    $('#contractsContainer').children().last().after(element);
                }

                $('#contractsContainer').children().last().on('touchend', function(){
                    goToSpecificContract(this);
                })



            }

            $.ajax({
                url:server+'/GigMe/api/contracts/getContractsCreatedByUser/'+getJData().ID,
                method:'GET',
                success:function(data,status,xhr){
                    createdContracts = data;
                    console.log("Contracts!!!!!");

                    console.log(data);
                    for (var i=0;i<data.length;i++){
                        var color = 'rgba(0,0,0,0);';
                        if (data[i].CreatorSigned.toString()=='true' && data[i].ClientSigned.toString()=='false'){
                            color = 'rgba(224, 249, 125, 0.49);'
                        }
                        else if (data[i].CreatorSigned.toString()=='true' && data[i].ClientSigned.toString()=='true'){
                            color = 'rgba(127, 249, 125, 0.49);';
                        }
                        var element = "<li name='contract' id='"+i+"' onclick='goToSpecificCreatedContract(this)' class='msgBlock' style='background-color:"+color+"'>" +
                            "<div>" +
                            "<h2>"+data[i].ContractTitle+"</h2>"+
                            "<p style='float:right;'> Proposed By "+data[i].Creator.Name+"</p>"+
                            "<p>"+data[i].ContractDescription+"</p>"+

                            "<p style='float:right; color:lawngreen'>"+data[i].Price+"</p>"+
                            "<p >"+data[i].ContractTime+"</p>"+
                            "</div>" +
                            "</li>";
                        if (i==0){
                            $('#createdContainer').append(element);
                        }
                        else{
                            $('#createdContainer').children().last().after(element);
                        }

                        $('#createdContainer').children().last().on('touchend', function(){
                            goToSpecificCreatedContract(this);
                        })



                    }

                },
                error:function(data,status,xhr){
                    alert('server error when getting media!')
                }
            });

        },
        error:function(data,status,xhr){
            alert('server error when getting media!')
        }
    });
};

goToContracts = function(){
    var nextpage = "./contracts.html";
    // swipe using id of next page if exists
    setScreenId("contractsPage");

    $.mobile.changePage(nextpage, {transition: "none", reverse: false}, true, false);
    //}
    event.handled = true;
};

function goToSpecificContract(contract){
    var contractID = parseInt($(contract).attr('id'));
    selectedContract = thoseContracts[contractID];

    var nextpage = "./contractReadView.html";
    // swipe using id of next page if exists
    setScreenId("contractView");

    $.mobile.changePage(nextpage, {transition: "none", reverse: false}, true, false);
    //}
    event.handled = true;

}

function goToSpecificCreatedContract(contract){
    var contractID = parseInt($(contract).attr('id'));
    selectedContract = createdContracts[contractID];

    var nextpage = "./contractCreatorView.html";
    // swipe using id of next page if exists
    setScreenId("contractCreatorView");

    $.mobile.changePage(nextpage, {transition: "none", reverse: false}, true, false);
    //}
    event.handled = true;

}

function handleSpecificContractView(){
    $('#contractTitle').text(selectedContract.ContractTitle);
    $('#description').text(selectedContract.ContractDescription);
    $('#time').text(selectedContract.ContractTime);
    $('#proposer').text('Proposed By ' + selectedContract.Creator.Name);
    $('#contractContent').text(selectedContract.ContractContent);
    $('#proposedPrice').val(selectedContract.Price);
    if (selectedContract.CreatorSigned){
        $('#creatorSigned').prop('checked',true)
    }
    else{
        $('#creatorSigned').prop('checked',false)
    }
    if (selectedContract.ClientSigned){
        $('#clientSigned').prop('checked',true)
    }
    else{
        $('#clientSignedLabel').css({display:'none'});
        $('#clientSigned').css({display:'none'});
        $('#signContractBtn').css({display:'block'});
    }
    $('#getPdfButton').attr('href',server+'/GigMe/api/contracts/getContractPDF/'+selectedContract.ContractID);
}


function handleSpecificContractCreatorView(){
    $('#contractTitle').text(selectedContract.ContractTitle);
    $('#description').text(selectedContract.ContractDescription);
    $('#time').text(selectedContract.ContractTime);
    $('#client').text('Proposed To ' + selectedContract.Client.Name);
    $('#contractContent').text(selectedContract.ContractContent);
    $('#proposedPrice').val(selectedContract.Price);
    if (selectedContract.ClientSigned){
        $('#clientSigned').prop('checked',true);
    }
    else{
        $('#clientSigned').prop('checked',false);
    }
    $('#getPdfButton').attr('href',server+'/GigMe/api/contracts/getContractPDF/'+selectedContract.ContractID);
}


function signContract(){
    $.ajax({
        url:server+'/GigMe/api/contracts/accept/'+selectedContract.ContractID+'/'+jdata.ID,
        method:'POST',
        success:function(data,status,xhr){
            alert('you have signed a contract!!!');
            history.go(-1);
            navigator.app.backHistory();

        },
        error:function(data,status,xhr){
            alert('server error when signing contract!')
        }
    });
}


