jdata=undefined;
server=undefined;
token=undefined;
var username=undefined;
var password=undefined;




//Visible objects
var notif;

function getNotif(){
	$('#notifications').text(notif);
	$('#nearUsers').text(nearByUsers);
}

function getId(){
	return jdata.Username;
}

function getJData(){
	return jdata;
}

function getToken(){
	return token;
}
function getServer(){
	return server;
}

function facebookLogin(){
	facebookConnectPlugin.login(["public_profile", "email"], function(){
			try{
				facebookConnectPlugin.api("/me?fields=email", ["public_profile", "email"],
					function (result) {
						username = result.email;
						password = result.id;
						loginFunction();

					}, function(){alert('error')});
			}
			catch(err){
				alert(err);
				alert(err.message);
			}
		},
		function loginError(error) {
			console.error(error)
		}
	);

}

$(function(){
	//tempLogin();
	setScreenId("loginPage");
	var un = window.localStorage.getItem('username');
	var pw = window.localStorage.getItem('password');
	var srv = window.localStorage.getItem('server');
	if ((un!=undefined && un!='')&&
		(pw!=undefined && pw!='')&&
		(server!=undefined && server!='')){
		loginFunction();

	}

	$("#login").click(function(){
		//$("#container").html($("#username").val()+","+$("#password").val());
		loginFunction();
	});
});

function loginFunction(){
	if (username==undefined || password==undefined){
		loginlogics($('#username').val(), $('#password').val());
	}
	else{
		loginlogics(username,password);
	}
}

function loginlogics(username,password){
	$.ajax({
		method:'GET',
		dataType: "json",
		url:"http://"+$("#serverip").val()+"/GigMe/api/user/login/"+
		username+"/"+
		password,
		success:function(data, status, xhr){
			window.localStorage.setItem('username',username);
			window.localStorage.setItem('password',password);
			server = "http://"+$("#serverip").val();
			window.localStorage.setItem('server',server);
			$("#container").html(data);
			var pageUrl1 = "./mainPage.html";
			token = xhr.getResponseHeader("Token");
			jdata = data;
			$.mobile.changePage(pageUrl1, { reloadPage : true, changeHash : false });

			startLocationFlow();
			getNotifications();
			fillNotificationsPanel();
			setScreenId("mainPage");
			console.log(jdata);
			//	handleUsersProfile();

		},
		error:function(data){
			$("#container").html(data.responseText);
			var errormsg = "Connection timeout.";
			if(data.length>1){
				errormsg = data.responseText;
			}
			alert(errormsg);
		},
		timeout: 3000
	});
}
