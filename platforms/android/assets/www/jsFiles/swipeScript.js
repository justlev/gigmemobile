var screen = "login";

function getScreenId(){
    return screen;
}

function setScreenId(screenId){
    screen = screenId;
}

$(document).on('swipeleft', '.ui-page', function(event){
    if(event.handled !== true) // This will prevent event triggering more then once
    {
        var nextScreenId;
        var nextpage;
        if(getScreenId() == "nearUsersMapPage"){
            nextpage = "./mainPage.html";
            nextScreenId = "mainPage";
        } else if(getScreenId() == "mainPage"){
            nextpage = "./profile.html";
            nextScreenId = "profilePage";
        }

        // swipe using id of next page if exists
        //alert("Trying to switch to: " + nextScreenId + " : " + nextpage + " from: " + getScreenId());
        if (nextpage.length > 0 || nextpage != undefined) {

            $.mobile.changePage(nextpage, {transition: "none", reverse: false}, true, false);
            setScreenId(nextScreenId);
        }
        event.handled = true;
    }
    return false;         
});

$(document).on('swiperight', '.ui-page', function(event){     
    if(event.handled !== true) // This will prevent event triggering more then once
    {
        var nextScreenId;
        var prevpage;
        if(getScreenId() == "mainPage"){
            prevpage = "./nearUsersMap.html";
            nextScreenId = "nearUsersMapPage";
        } else  if(getScreenId() == "profilePage"){
            prevpage = "./mainPage.html";
            nextScreenId = "mainPage";
        } else if(getScreenId() == "messagesPage"){
            prevpage = "./profile.html";
            nextScreenId = "profilePage";
        } else if(getScreenId() == "conversationPage"){
            prevpage = "./messages.html";
            nextScreenId = "messagesPage";
        }
       // alert("Trying to switch to: " + nextScreenId + " : " + prevpage + " from: " + getScreenId());
        if (prevpage.length > 0 || prevpage != undefined) {

            $.mobile.changePage(prevpage, {transition: "none", reverse: true}, true, false);
            setScreenId(nextScreenId);
        }

        event.handled = true;
    }
    return false;            
});

//touch
goToProfile=function(){


    var nextpage = "./profile.html";
    // swipe using id of next page if exists

    $.mobile.changePage(nextpage, {transition: "none", reverse: false}, true, false);
    setScreenId("profilePage");
    event.handled = true;

};

$("#notificationsContainer").on("touchend", function () { goToProfile();});
$('#notsContainer').on('touchend', function(){
    goToProfile();
});


//Menu buttons handlers:

