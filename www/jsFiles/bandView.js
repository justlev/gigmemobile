/**
 * Created by lev on 19/01/16.
 */

var bandMembers;

function goToBands(){
    var nextpage = "./bandView.html";
    // swipe using id of next page if exists
    setScreenId("bandViewPage");

    $.mobile.changePage(nextpage, {transition: "none", reverse: false}, true, false);
    //}
    event.handled = true;
}

function handleBandViewPage(){

    $('#bandName').text(selectedBand.BandName);
    $('#description').text(selectedBand.Description);
    var preferences = selectedBand.Preferences;
    var textToWrite = "";
    for (var i=0;i<preferences.length;i++){
        textToWrite+=preferences[i].Name;
        if (i!=preferences.length-1){
            textToWrite+=", ";
        }
    }
    $('#bandProfilePicture').attr('src', server+"/GigMe/api/bands/getBandsImage/"+selectedBand.BandID);
    $('#preferences').text("Playing "+textToWrite);
    if (selectedBand.Owner.ID==getJData().ID){
        $('#ownerhref').text("Owned by You");
    }
    else{
        $('#ownerhref').text("Owned by "+selectedBand.Owner.Name);
    }

    bandMembers = selectedBand.Members;
    var members = selectedBand.Members;
    for (var i=0;i<members.length;i++){
        var element = "<img class='band' onclick='selectedProfile(this)' id='"+i+"' src='"+server+"/GigMe/api/user/getUsersImage/"+members[i].ID+"' >";
        if (i==0){
            $('#membersUL').append(element);
        }
        else{
            $('#membersUL').children().last().after(element);
        }
    }
    $.ajax({
        url:server+'/GigMe/api/media/getUsersMedia/'+selectedBand.BandID,
        method:'GET',
        success:function(data,status,xhr){
            console.log("Medias!!!!!");

            console.log(data);
            for (var i=0;i<data.length;i++){

                var mediaPlayer = "SoundCloudPlayer";
                if (data[i].mediaPath.toLowerCase().indexOf("bandcamp")>0){
                    mediaPlayer = "https://bandcamp.com/EmbeddedPlayer/track=2717629732/size=small/bgcol=ffffff/linkcol=7137dc/artwork=none/transparent=true/";
                }

                $('#postContainerUL').append("<li name='usersMedia'>" +
                    "<div id='media"+i+"' class='post'>" +
                    "<p>"+data[i].mediaTitle+"</p>"+
                    '<iframe style="border: 0; width: 100%; height: 42px;" src="'+mediaPlayer+'" seamless><a href="'+data[i].mediaPath+'">'+data[i].mediaTitle+'</a></iframe>'+
                    "</div>" +
                    "</li>")
            }

        },
        error:function(data,status,xhr){
            alert('server error when getting media!')
        }
    });


}

function goToBandOwner(){
    goToSelectedProfile(selectedBand.Owner);
}

function selectedProfile(element){
    var index = parseInt($(element).attr('id'));
    var thisMember = bandMembers[index];
    goToSelectedProfile(thisMember);
}


