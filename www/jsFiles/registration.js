/**
 * Created by lev on 28/01/16.
 */

var selectedPreferences = [];
var preferences;

var selectedInstruments= [];
var instruments;

function selectedPreference(item){
    var id = parseInt($(item).attr('id'));
    if ($(item).val()==1){
        $(item).css({'background-color':'rgba(165, 241, 172, 0)'});
        var index = selectedPreferences.indexOf(preferences[id]);
        selectedPreferences.splice(index,1);
        $(item).val(0);
    }
    else{
        $(item).val(1);
        selectedPreferences.push(preferences[id]);
        $(item).css({'background-color':'rgba(165, 241, 172, 0.52)'})
    }


}

function selectedInstrument(item){
    var id = parseInt($(item).attr('id'));
    if ($(item).val()==1){
        $(item).css({'background-color':'rgba(165, 241, 172, 0)'});
        var index = selectedInstruments.indexOf(instruments[id]);
        selectedInstruments.splice(index,1);
        $(item).val(0);
    }
    else{
        $(item).val(1);
        selectedInstruments.push(instruments[id]);
        $(item).css({'background-color':'rgba(165, 241, 172, 0.52)'})
    }


}

function clickedNext(){
    $('#registrationForm').fadeOut('fast', function(){
        $('#part2').fadeIn('fast');
        $('#registerBtn').fadeIn('fast');
    })
}

function goBack(){
    $('#part2').fadeOut('fast', function(){
        $('#registerBtn').fadeOut('fast');
        $('#registrationForm').fadeIn('fast')
        ;
    })

};

function regularRegistrationStart(){
    $('#fbRegistrationBtn').fadeOut('fast');
    $('#regularReg').fadeOut('fast');
    $('#orP').fadeOut('fast');
    $('#registrationForm').fadeIn('fast');
}

function register(){

    server = "http://"+$("#serverip").val();
    var bday = moment($('#birthday').val()).format('YYYY-MM-DD');
    $.ajax({
        url:server+"/GigMe/api/user/createUser/"+$('#newPassword').val()+'/'+$('#fullName').val()+'/'+$('#email').val()+'/'+bday+'/'+$('#phoneNumber').val()+'/'+$('#address').val(),
        method:'POST',
        success:function(data,xhr,something){
            token = xhr.getResponseHeader("Token");
            var insts;
            var genres;
            for (var i=0;i<selectedInstruments.length;i++){
                insts+=selectedInstruments[i].ID;
                if (i<selectedInstruments.length-1){
                    insts+=',';
                }
            }

            if (selectedInstruments.length>0){
                $.ajax({
                    url:server+"/GigMe/api/user/addInstruments/"+insts+'/'+data.ID,
                    method:'POST',
                    success:function(data,xhr,something){
                    },
                    error:function(){
                        alert('error adding instruments');
                    }
                });
            }

            for (var i=0;i<selectedPreferences.length;i++){
                genres+=selectedPreferences[i].ID;
                if (i<selectedPreferences.length-1){
                    genres+=',';
                }
            }

            if (selectedPreferences.length>0){
                $.ajax({
                    url:server+"/GigMe/api/user/addPreferences/"+genres+'/'+data.ID,
                    method:'POST',
                    success:function(data,xhr,something){
                    },
                    error:function(){
                        alert('error adding genres');
                    }
                });
            }

            function fail(error){
                alert("An error has occured: Code " + error.code);
                console.log("Upload error source " + error.source);
                console.log("Upload error target " + error.target);
            }

            function win(){
               // alert('win!!!!');
                window.localStorage.setItem('username',$("#email").val());
                window.localStorage.setItem('password',$("#newPassword").val());
                window.localStorage.setItem('server',server);
                var pageUrl1 = "./mainPage.html";



               // alert('GigMetoken! '+token)
                jdata = data;
                //alert(JSON.stringify(jdata));

                startLocationFlow();
                getNotifications();
                fillNotificationsPanel();
                setScreenId("mainPage");

                $.mobile.changePage(pageUrl1, { reloadPage : true, changeHash : false });

            }

            if ($('#profilePic').attr('src').indexOf('graph.facebook')<0){
                var fileURI = regProfileUri;
                console.log('image url: '+fileURI);
                console.log('beforeFileUploadOptions');
                var options = new FileUploadOptions();
                options.fileKey = "file";
                console.log('beforeSplitURL');
                options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
                console.log(options.fileName);
                options.mimeType = "multipart/form-data";
                console.log('beforeFT');
                var ft = new FileTransfer();
                console.log('starting sending picture');
                console.log(ft);
                ft.upload(fileURI, server+"/GigMe/api/user/updateProfilePicture/"+data.ID, win, fail, options);
            }
            else{
                try{
                    var img = document.getElementById('profilePic');
                    var canvas = document.createElement('canvas');
                    var ctx = canvas.getContext('2d');
                    canvas.width = img.width;
                    canvas.height = img.height;
                    ctx.drawImage(img, 0, 0);
                    var dataUrl = canvas.toDataURL('image/png');
                    var blob = dataUriToBlob(dataUrl);

                    try{
                        var form = new FormData();
                        var xhr = new XMLHttpRequest();
                        xhr.open('POST', server+"/GigMe/api/user/updateProfilePicture/"+data.ID, true);    // plug-in desired URL
                        xhr.onreadystatechange = function() {
                            if (xhr.readyState == 4) {
                                if (xhr.status == 200) {
                                    //alert('Success: ' + xhr.responseText);
                                    win();
                                } else {
                                    alert('Error submitting image: ' + xhr.status);
                                }
                            }
                        };
                        //form.append('file', dataUrl);
                        form.append('file', blob);
                        xhr.send(form);
                    }
                    catch(err2){
                        alert('error trying to send AJAX, will now try the uploader.: '+err2.message);
                        ft.upload(dataUrl, server+"/GigMe/api/user/updateProfilePicture/"+data.ID, win, fail, options);
                    }
                    // submit as a multipart form, along with any other data


                    function dataUriToBlob(dataURI) {
                        // serialize the base64/URLEncoded data
                        var byteString;
                        if (dataURI.split(',')[0].indexOf('base64') >= 0) {
                            byteString = atob(dataURI.split(',')[1]);
                        }
                        else {
                            byteString = unescape(dataURI.split(',')[1]);
                        }

                        // parse the mime type
                        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

                        // construct a Blob of the image data
                        var array = [];
                        for(var i = 0; i < byteString.length; i++) {
                            array.push(byteString.charCodeAt(i));
                        }
                        return new Blob(
                            [new Uint8Array(array)],
                            {type: mimeString}
                        );
                    }
                }
                catch(err){
                    alert(err);
                    alert(err.message);
                }


                /*
                var dataToSend = {
                    propertyName:'profilePic',
                    newValue:$('#profilePic').attr('src'),
                    id:data.ID
                };
                $.ajax({
                    url:server+"/GigMe/api/user/updateProfilePictureFromURL",
                    method:'POST',
                    data:JSON.stringify(dataToSend),
                    success:function(data,xhr,something){
                        win();
                    },
                    error:function(){
                        fail();
                    }
                });
                */
            }



        }
    });

}


function handleRegistrationsPage(){

    server = "http://"+$("#serverip").val();

    $.ajax({
        url:server+"/GigMe/api/preferences/getAllPreferences",
        method:'GET',
        success:function(data,xhr,something){
            preferences = data;
            for (var i=0;i<data.length;i++){
                var element = "<li class='genre' onclick='selectedPreference(this)' id='"+i+"'><div class='genreDiv'>"+data[i].Name+"</div></li>";
                if (i==0){
                    $('#preferences').append(element);
                }
                else{
                    $('#preferences').children().last().after(element);
                }

                // $('#preferences').children().last().on('touchend',function(){selectedPreference(this)});
            }
            $.ajax({
                url:server+"/GigMe/api/instruments/getAllInstruments",
                method:'GET',
                success:function(data,xhr,something){
                    instruments = data;
                    for (var i=0;i<data.length;i++){
                        var element = "<li class='genre' onclick='selectedInstrument(this)' id='"+i+"'><div class='genreDiv'>"+data[i].Name+"</div></li>";
                        if (i==0){
                            $('#instruments').append(element);
                        }
                        else{
                            $('#instruments').children().last().after(element);
                        }

                        // $('#preferences').children().last().on('touchend',function(){selectedPreference(this)});
                    }

                }
            });
        }
    });

}

var regProfileUri;

function udpateRegistrationProfilePic(){
    navigator.camera.getPicture(function(uri){
        console.log(uri);
        $('#profilePic').attr("src",uri);

        regProfileUri = uri;



    }, function(error){
        alert(error);
        console.log(error);
    }, { quality: 75,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        encodingType: Camera.EncodingType.JPEG,
        mediaType:Camera.MediaType.PICTURE,
        targetWidth: 190,
        targetHeight: 190});
}