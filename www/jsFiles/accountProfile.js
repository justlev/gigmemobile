/**
 * Created by lev on 14/01/16.
 */

    var bands;
var selectedBand;

var loadUsersData = function(){
    alert('ass');
    console.log("opened that profile page");
    console.log(getJData());
    console.log(token);


};

$('#messagesBtn').on('touchend', function(){
    goToMessages();
});

function handleUsersProfile(){
    $('#profilePic').attr('src',server+'/GigMe/api/user/getUsersImage/'+getJData().ID);
    $('#profileName').text(getJData().Name);
    var instruments = getJData().Instruments;
    var instrumentsStr = "";
    for (var i=0;i<instruments.length;i++){
        instrumentsStr+=instruments[i];
        if (i!=instruments.length-1){
            instrumentsStr+=", ";
        }
    }
    $('#instruments').text(instrumentsStr);
    $('#availableLocation').text(getJData().Address);

    $.ajax({
        url:server+'/GigMe/api/bands/getBands/'+getJData().ID,
        method:'GET',
        success:function(data,status,xhr){
            bands = data;
            console.log("Bands!!!!");
            console.log(data);
            for (var i=0;i<data.length;i++){
                var element = "<li name='usersBand' onclick='clickedOnBand(this)' id='"+i+"'><img class='band' src='"+server+"/GigMe/api/bands/getBandsImage/"+data[i].BandID+"'></li>";
                if (i==0){
                    $('#bands').append(element)
                }
                else{
                    $('#bands').children().last().after(element);
                    $('#bands').children().last().on('touchend', function(){
                        clickedOnBand(this);
                    });
                }


            }

        },
        error:function(data,status,xhr){
            alert('server error when getting bands')
        }
    });

    $.ajax({
        url:server+'/GigMe/api/bands/getBandsByMember/'+getSelectedProfile().ID,
        method:'GET',
        success:function(data,status,xhr){
            bands = data;
            console.log("Bands!!!!");
            console.log(data);
            for (var i=0;i<data.length;i++){
                var element = "<li name='usersBand' onclick='clickedOnBand(this)' id='"+i+"'><img class='band' src='"+server+"/GigMe/api/bands/getBandsImage/"+data[i].BandID+"'></li>";
                if (i==0){
                    if (data[i].Owner.ID==getSelectedProfile().ID){
                        $('#otherBands').append(element)
                    }
                    else{
                        $('#otherBands').append(element)
                    }

                }
                else{

                    if (data[i].Owner.ID==getSelectedProfile().ID){
                        $('#otherBands').children().last().after(element);
                    }
                    else{
                        $('#otherBands').children().last().after(element);
                    }

                    /*$('#bands').children().last().on('touchend', function(){
                     clickedOnBand(this);
                     });*/
                }


            }

        },
        error:function(data,status,xhr){
            alert('server error when getting bands')
        }
    });

    $.ajax({
        url:server+'/GigMe/api/media/getUsersMedia/'+getJData().ID,
        method:'GET',
        success:function(data,status,xhr){
            console.log("Medias!!!!!");

            console.log(data);
            for (var i=0;i<data.length;i++){

                var mediaPlayer = "SoundCloudPlayer";
                if (data[i].mediaPath.toLowerCase().indexOf("bandcamp")>0){
                    mediaPlayer = "https://bandcamp.com/EmbeddedPlayer/track=2717629732/size=small/bgcol=ffffff/linkcol=7137dc/artwork=none/transparent=true/";
                }

                $('#postContainerUL').append("<li name='usersMedia'>" +
                    "<div id='media"+i+"' class='post'>" +
                    "<p>"+data[i].mediaTitle+"</p>"+
                    '<iframe style="border: 0; width: 100%; height: 42px;" src="'+mediaPlayer+'" seamless><a href="'+data[i].mediaPath+'">'+data[i].mediaTitle+'</a></iframe>'+
                    "</div>" +
                    "</li>")
            }

        },
        error:function(data,status,xhr){
            alert('server error when getting media!')
        }
    });

fillNotificationsPanel();


}

function clickedOnBand(b){
    selectedBand = bands[parseInt($(b).attr('id'))];
    goToBands();
}

function updateUsersProfilePic(){
    navigator.camera.getPicture(function(uri){
        console.log(uri);
        $('#profilePic').attr("src",uri);
        imageURI = uri;


        function fail(error){
            alert("An error has occured: Code " + error.code);
            console.log("Upload error source " + error.source);
            console.log("Upload error target " + error.target);
        }

        function win(){
            goToProfile();
        }

        var fileURI = imageURI;
        console.log('image url: '+fileURI);
        console.log('beforeFileUploadOptions');
        var options = new FileUploadOptions();
        options.fileKey = "file";
        console.log('beforeSplitURL');
        options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
        console.log(options.fileName);
        options.mimeType = "multipart/form-data";
        console.log('beforeFT');
        var ft = new FileTransfer();
        console.log('starting sending picture');
        console.log(ft);
        ft.upload(fileURI, server+"/GigMe/api/user/updateProfilePicture/"+jdata.ID, win, fail, options);


    }, function(error){
        console.log(error);
    }, { quality: 75,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        encodingType: Camera.EncodingType.JPEG,
        mediaType:Camera.MediaType.PICTURE,
        targetWidth: 190,
        targetHeight: 190});
}