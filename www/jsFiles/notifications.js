/**
 * Created by lev on 26/01/16.
 */
var notifications;

function fillNotificationsPanel(){
    $.ajax({
        method:'GET',
        url:server+'/GigMe/api/notifications/getUsersNotifications/'+getJData().ID,
        success:function(data){
            notifications = data;
            for (var i=0;i<data.length;i++){
                if (!data[i].NotificationRead){
                    var element = '<div class="notification" style="border: 1px;border-style: groove;" id="'+data[i].NotificationID+'" onclick="selectedNotification(this)">' +
                        '<p style="font-weight: bold;">'+data[i].NotificationText+'</p>' +
                        '<label style="" >'+data[i].NotificationDate+'</label>' +
                        '</div>'
                }
                else{
                    var element = '<div class="notification" style="border: 1px;border-style: groove;" id="'+data[i].NotificationID+'" onclick="selectedNotification(this)">' +
                        '<p>'+data[i].NotificationText+'</p>' +
                        '<label style="" >'+data[i].NotificationDate+'</label>' +
                        '</div>'
                }

                    $('#notificationsPanel').append(element);
            }
        },
        error:function(xhr,data,status){
            alert('error getting notifications!');
            console.log(xhr);
            console.log(data);
            console.log(status);
        }
    });

}

function selectedNotification(not){
    var index = $(not).attr("id");

    var selenot;
    for (var i=0;i<notifications.length;i++){
        if (notifications[i].NotificationID==index){
            selenot = notifications[i];
        }
    }
    if (!selenot.NotificationRead){
        $.ajax({
            method:'POST',
            url:server+'/GigMe/api/notifications/setNotificationRead/'+selenot.NotificationID,
            success:function(){
                $($(not).children()[0]).css({"font-weight":"normal"});
            },
            error:function() {
                console.log('error setting notification to read.')
            }

        });
    }

    //TODO: GoToNotification xD


}