
var map;
var userLocation;
var markers;
var markersApplied = false;

function goToMaps(){
    prevpage = "./nearUsersMap.html";
    nextScreenId = "nearUsersMapPage";
    $.mobile.changePage(prevpage, {transition: "none", reverse: true}, true, false);
    setScreenId(nextScreenId);
    event.handled = true;
}

function setUserLocation(newX, newY){
    userLocation = {latitude: newX, longitude: newY};
    var latlng = new google.maps.LatLng(newX,newY);
    //init map
    mapOptions ={
        center: latlng,
        zoom: 13

    };
    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    if(!markersApplied && userLocation!=undefined){
        getUserMarkers();
        markersApplied = true;
    }

    if(map != undefined && map != null && !markersApplied){
        getUserMarkers();
        markersApplied = true;
    }
}
function initMap() {
    //init position
    var x = 31.2535598;
    var y = 34.7769184;
    if (myCurrentPosition!=undefined){
        x = myCurrentPosition.coords.latitude;
        y = myCurrentPosition.coords.longitude;
    }
    var latlng = new google.maps.LatLng(x,y);
    //init map
    mapOptions ={
        center: latlng,
        zoom: 13

    };
    var pinColor = "66ff33";
    var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
        new google.maps.Size(21, 34),
        new google.maps.Point(0,0),
        new google.maps.Point(10, 34));
    var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
        new google.maps.Size(40, 37),
        new google.maps.Point(0, 0),
        new google.maps.Point(12, 35));
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(x, y),
        map: map,
        title: "You",
        icon:pinImage,
        shadow:pinShadow
    });
    marker.setMap(map);

    if(!markersApplied && userLocation!=undefined){
        getUserMarkers();
        markersApplied = true;
    }
}

var usersOnMap;
function getUserMarkers(){
    var request = "locations/getNearByUsers/"+userLocation.latitude+"/"+userLocation.longitude+"/"+1+"/"+getJData().ID;
    $.ajax({
        method:'GET',
        dataType: "json",
        url:server+"/GigMe/api/"+request,
        headers:{
            "Token":getToken()
        },
        success:function(data){
            console.log("Successfully received response from: " + request);
            console.log(data);
            usersOnMap = data;
            createMarkers(data);

        },
        error:function(data){
            console.log("Failed requesting: " + request);
            console.log(data);
        },
        timeout: 10000
    });

}
function createMarkers(users){
    if(users != undefined) {
        $.each(users, function (i, userI) {
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(userI.LocationXY.LocationX, userI.LocationXY.LocationY),
                map: map,
                title: userI.Name
            });

            var contentString = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<h1 id="firstHeading" class="firstHeading">'+userI.Name+'</h1>'+
                '<div id="bodyContent">'+
                '<p>Address: '+userI.Address+' <br>' +
                'Age: '+userI.Age+' ' +
                'Link: <a id="'+i+'" onclick="goToSelectedProfileFromMap(this)" >Go to profile</a>' +
                '</p>'+
                '</div>'+
                '</div>';
            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker.addListener('click', function() {
                infowindow.open(map, marker);
            });


            marker.setMap(map);
            //console.log("Marker created" + i);
        });
    }
}

function goToSelectedProfileFromMap(elem){
    var index = parseInt($(elem).attr('id'));
    goToSelectedProfile(usersOnMap[index]);
}