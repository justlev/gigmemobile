/**
 * Created by 8o8inSquares on 1/17/2016.
 */

var unreadNotifications;
var unreadNotificationsCount;

$('#notsContainer').on('touchend', function(){
    goToProfile();
});


$('#messagesBtn').on('touchend', function(){
    goToMessages();
});


function getNotifications(){
    var jnot = 0;
    $.ajax({
        method:'GET',
        dataType: "json",
        url:getServer()+"/GigMe/api/notifications/getUsersUnreadNotifications/"+jdata.ID,
        headers:{
            "Token":getToken()
        },
        success:function(data){
            unreadNotifications = data;
            unreadNotificationsCount = data.length;
            console.log("notifications");
            console.log(data);
            $('#notifications').text(data.length);
            notif = data.length;
            setTimeout(function(){notificationsListener()},5000);
        },
        error:function(data){
            console.log("notifications error");
            console.log(data);
            var errormsg = "Connection timeout.";
            if(data>1){
                errormsg = data.responseText;

            }
            jnot = errormsg;
        },
        timeout: 3000
    });
}

function notificationsListener(){
    $.ajax({
        method:'GET',
        dataType: "json",
        url:getServer()+"/GigMe/api/notifications/getNotificationsCount/"+jdata.ID,
        timeout: 3000,
        headers:{
            "Token":getToken()
        },
        success:function(data){
            setTimeout(function(){notificationsListener();},10000);
            if (data>unreadNotificationsCount){
                $.ajax({
                    method:'GET',
                    dataType: "json",
                    url:getServer()+"/GigMe/api/notifications/getUsersUnreadNotifications/"+jdata.ID,
                    headers:{
                        "Token":getToken()
                    },
                    success:function(data){

                        notif = data.length;
                        for (var i=unreadNotificationsCount;i<data.length;i++){
                            var element = '<div class="notification" style="border: 1px;border-style: groove;" id="'+data[i].NotificationID+'" onclick="selectedNotification(this)">' +
                                '<p>'+data[i].NotificationText+'</p>' +
                                '<label style="" >'+data[i].NotificationDate+'</label>' +
                                '</div>';
                            $('#notificationsPanel').append(element);
                            notifications.push(data[i]);
                        }

                        unreadNotifications = data;
                        unreadNotificationsCount = data.length;
                        $('#notifications').text(data.length);
                    },
                    error:function(data){
                        console.log("notifications error");
                        console.log(data);
                        var errormsg = "Connection timeout.";
                        if(data>1){
                            errormsg = data.responseText;

                        }
                        jnot = errormsg;
                    },
                    timeout: 3000
                });
            }


        },
        error:function(data){
        console.log('notification listener error');
        },

    });
}