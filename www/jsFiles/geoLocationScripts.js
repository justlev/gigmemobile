/**
 * Created by 8o8inSquares on 1/17/2016.
 */
//near by users
var nearByUsers;
var myCurrentPosition=undefined;

var successCallback=function(position){
    myCurrentPosition= position;
    console.log(position);
    $.ajax({
        method:'GET',
        dataType: "text/plain",
        url:server+"/GigMe/api/locations/getNearByUsersCount/"+position.coords.latitude+"/"+position.coords.longitude+"/"+0.5+"/"+jdata.ID,
        success:function(data){
            console.log("Success nearby discovery");
            console.log(data);
            $('#nearUsers').text(data);
            nearByUsers = data;
            setUserLocation(position.coords.latitude,position.coords.longitude);
        },
        error:function(data){
            if (data.status==200){
                console.log("Fail nearby discovery");
                console.log(data);
                $('#nearUsers').text(data.responseText);
                setUserLocation(position.coords.latitude,position.coords.longitude);
                return;
            }
            console.log("error");
            console.log(data);
            var errormsg = "Connection timeout.";
            if(data>1){
                errormsg = data.responseText;

            }
            jnot = errormsg;
        },
        timeout: 10000
    });

};

function startLocationFlow(){
    navigator.geolocation.getCurrentPosition(successCallback, errorCallback, {
        enableHighAccuracy: false,
        timeout: 10000
    });
}

function getNearByUsers(){
    return nearByUsers;
}
function setNearByUsers(nbu){
    nearByUsers = nbu;
}