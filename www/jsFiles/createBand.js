/**
 * Created by lev on 19/01/16.
 */
function goToBandCreation(){
    var nextpage = "./createBandView.html";
    // swipe using id of next page if exists
    setScreenId("createBandPage");

    $.mobile.changePage(nextpage, {transition: "none", reverse: false}, true, false);
    //}
    event.handled = true;
}

function handleBandCreationPage(){
    console.log('handling band creation...');
   // $('#selectProfilePicBtn').on('touchend', function(){
     //   selectBandProfilePic();
    //});
    $.ajax({
        url:server+"/GigMe/api/preferences/getAllPreferences",
        method:'GET',
        success:function(data,xhr,something){
            preferences = data;
            for (var i=0;i<data.length;i++){
                var element = "<li class='genre' onclick='selectedPreference(this)' id='"+i+"'><div class='genreDiv'>"+data[i].Name+"</div></li>";
                if (i==0){
                    $('#preferences').append(element);
                }
                else{
                    $('#preferences').children().last().after(element);
                }

               // $('#preferences').children().last().on('touchend',function(){selectedPreference(this)});
            }

        }
    });

}

var selectedPreferences = [];
var preferences;
var imageURI;

function selectedPreference(item){
    var id = parseInt($(item).attr('id'));
    if ($(item).val()==1){
        $(item).css({'background-color':'rgba(165, 241, 172, 0)'});
        var index = selectedPreferences.indexOf(preferences[id]);
        selectedPreferences.splice(index,1);
        $(item).val(0);
    }
    else{
        $(item).val(1);
        selectedPreferences.push(preferences[id]);
        $(item).css({'background-color':'rgba(165, 241, 172, 0.52)'})
    }


}

function selectBandProfilePic(){
        // Retrieve image file location from specified source
        navigator.camera.getPicture(function(uri){
            console.log(uri);
            $('#bandProfilePic').attr("src",uri);
            imageURI = uri;

        }, function(error){
            console.log(error);
        }, { quality: 75,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            encodingType: Camera.EncodingType.JPEG,
            mediaType:Camera.MediaType.PICTURE,
            targetWidth: 190,
            targetHeight: 190});

}

function addPreference(){
    $('#preferencesForm').css({display:'block'})
}

function saveBand(){
    var bandName = $('#bandName').val();
    var bandDescription = $('#bandDescription').val();
    if (bandName==''){
        alert('Please enter a name for your band');
        return;
    }
    if (bandDescription==''){
        alert('Please enter a short description for your band');
        return;
    }
    if ($('#bandProfilePic').attr("src")==undefined || $('#bandProfilePic').attr('src').indexOf('overCSS/Profilepic.j')>-1 ){
        alert('please choose a profile picture for your band');
        return;
    }
    var prefs='';
    for (var i=0;i<selectedPreferences.length;i++){
        prefs+=selectedPreferences[i].ID;
        if (i<selectedPreferences.length-1){
            prefs+=',';
        }
    }
    $.ajax({
        url:server+"/GigMe/api/bands/addBand/"+jdata.ID+"/"+bandName+'/'+bandDescription+'/'+prefs,
        method:'POST',
        contentType:'text/plain',
        error:function(xhr,data,status){
            if (xhr.statusCode==200){
                var bandID = xhr.responseText;
                console.log(bandID);


                function fail(error){
                    alert("An error has occured: Code " + error.code);
                    console.log("Upload error source " + error.source);
                    console.log("Upload error target " + error.target);
                }

                function win(){
                    goToProfile();
                }

                var fileURI = imageURI;
                console.log('image url: '+fileURI);
                console.log('beforeFileUploadOptions');
                var options = new FileUploadOptions();
                options.fileKey = "file";
                console.log('beforeSplitURL');
                options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
                console.log(options.fileName);
                options.mimeType = "multipart/form-data";
                console.log('beforeFT');
                var ft = new FileTransfer();
                console.log('starting sending picture');
                console.log(ft);
                ft.upload(fileURI, server+"/GigMe/api/bands/updateBandPicture/"+bandID, win, fail, options);

            }

        },
        success:function(data,xhr,something){
            console.log('success');
            var id = data;

            function fail(error){
                alert("An error has occured: Code " + error.code);
                console.log("Upload error source " + error.source);
                console.log("Upload error target " + error.target);
            }

            function win(){
                goToProfile();
            }



            var fileURI = imageURI;
            console.log('image url: '+fileURI);
            console.log('beforeFileUploadOptions');
            var options = new FileUploadOptions();
            options.fileKey = "file";
            console.log('beforeSplitURL');
            options.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
            console.log(options.fileName);
            options.mimeType = "multipart/form-data";
            console.log('beforeFT');
            var ft = new FileTransfer();
            console.log('starting sending picture');
            console.log(ft);
            ft.upload(fileURI, server+"/GigMe/api/bands/updateBandPicture/"+id, win, fail, options);

        }
    });
}
